#!/usr/bin/env bash

function add_vagrant_key {
  homedir=$(su - "$1" -c "echo $HOME")
  mkdir -p "$homedir"/.ssh
  curl -L 'https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub' -o "$homedir"/.ssh/authorized_keys2
  chown -Rf "$1". "$homedir"/.ssh
  chmod 700 "$homedir"/.ssh
  chmod 600 "$homedir"/.ssh/authorized_keys2
}

### APT-GET
apt update
apt -y install curl openssh-server sudo facter aptitude python3 python3-apt

### SSH
echo "UseDNS no" >> /etc/ssh/sshd_config
echo "GSSAPIAuthentication no" >> /etc/ssh/sshd_config

### Network
if [ "$(facter virtual)" = "virtualbox" ]; then
  [ -f /etc/udev/rules.d/70-persistent-net.rules ] && rm /etc/udev/rules.d/70-persistent-net.rules
  [ -f /lib/udev/rules.d/75-persistent-net-generator.rules ] && rm /lib/udev/rules.d/75-persistent-net-generator.rules
  rm -rf /dev/.udev/ /var/lib/dhcp/*
  echo "pre-up sleep 2" >> /etc/network/interfaces
fi

### Sudo
echo 'vagrant ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/vagrant
chmod 440 /etc/sudoers.d/vagrant
echo "Defaults !requiretty" >> /etc/sudoers

### Vagrant user
if [ "$(grep -c vagrant /etc/passwd)" == "0" ]; then
  useradd vagrant -m
fi

add_vagrant_key vagrant
