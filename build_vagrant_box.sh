#!/usr/bin/env bash

export _root_dir
_root_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

export _packer="packer"
export _vagrant="vagrant"
export _upload_to_vagrant_cloud="0"

msg() {
  echo >&2 -e "${1-}"
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  if [ -d "$_root_dir/output-debian" ]; then
    rm -r "$_root_dir/output-debian"
  fi
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

_checkPackerInstallation() {
  if ! which "$_packer" > /dev/null 2>&1; then
    die "Packer not install"
  fi
}

_checkVagrantInstallation() {
  if ! which "$_vagrant" > /dev/null 2>&1; then
    die "Vagrant not install"
  fi
}

_readVariables() {

  # Get variables from variables.pkr.hcl
  _major_version="$($_packer inspect variables.pkr.hcl | awk -F ': ' '$0 ~ /var.debian_major_version/{print $2}' | sed 's/"//g')"
  _version="$($_packer inspect variables.pkr.hcl | awk -F ': ' '$0 ~ /var.debian_version/{print $2}' | sed 's/"//g')"
  _release="$($_packer inspect variables.pkr.hcl | awk -F ': ' '$0 ~ /var.debian_release/{print $2}' | sed 's/"//g')"

  # Get variables from vagrant cloud
  _remote_latest_version="$($_vagrant cloud search --no-color --json "maxhamon/debian${_major_version}" | jq -r .[].version)"

  [ -z "$_major_version" ] && die "Cannot get major_version in variables.pkr.hcl"
  [ -z "$_version" ] && die "Cannot get version in variables.pkr.hcl"
  [ -z "$_release" ] && die "Cannot get release name in variables.pkr.hcl"
  [ -z "$_remote_latest_version" ] && die "Cannot get latest version from vagrant cloud"

}

_buildBox() {
  local _build_options=''

  msg "Build $_version"

  if [ $_upload_to_vagrant_cloud -ne 0 ]; then
    # shellcheck disable=SC2089
    _build_options='-except="vagrant-cloud"'
  fi

  # shellcheck disable=SC2090
  $_packer build $_build_options "$_root_dir"/
  export _rc=$?
}

_checkPackerInstallation
_checkVagrantInstallation
_readVariables

msg "To build : Debian $_version"
msg "Vagrant cloud latest version : $_remote_latest_version"

if [ "$_version" == "$_remote_latest_version" ]; then
  msg "Deactive upload to vagrant cloud"
  _upload_to_vagrant_cloud="1"
fi

if [ -f "$_root_dir/token" ]; then
  # shellcheck disable=SC1091
  source "$_root_dir/token"
fi

_buildBox

if [ $_rc -ne 0 ]; then
  cleanup
  die "Ended with error(s)" $_rc
fi

exit 0
