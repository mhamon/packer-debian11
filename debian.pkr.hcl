source "virtualbox-iso" "debian" {
  boot_command            = ["<esc><wait>", "install <wait>", "preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg <wait>", "debian-installer/language=en <wait>", "debian-installer/country=US <wait>", "auto <wait>", "locale=en_US <wait>", "kbd-chooser/method=us <wait>", "keyboard-configuration/xkb-keymap=us <wait>", "netcfg/get_hostname={{ .Name }} <wait>", "netcfg/get_domain=vagrantup.com <wait>", "fb=false <wait>", "debconf/frontend=noninteractive <wait>", "console-setup/ask_detect=false <wait>", "console-keymaps-at/keymap=us <wait>", "<enter><wait>"]
  boot_wait               = "5s"
  disk_size               = 32768
  guest_additions_path    = "VBoxGuestAdditions_{{ .Version }}.iso"
  guest_os_type           = "Debian_64"
  headless                = true
  http_directory          = "http"
  iso_checksum            = "${var.debian_iso_checksum}"
  iso_url                 = "http://cdimage.debian.org/cdimage/archive/${var.debian_version}/amd64/iso-cd/debian-${var.debian_version}-amd64-netinst.iso"
  shutdown_command        = "echo 'vagrant'|sudo -S /sbin/shutdown -hP now"
  ssh_password            = "vagrant"
  ssh_port                = 22
  ssh_username            = "vagrant"
  ssh_wait_timeout        = "10000s"
  vboxmanage              = [["modifyvm", "{{ .Name }}", "--memory", "4096"], ["modifyvm", "{{ .Name }}", "--cpus", "4"], ["modifyvm", "{{.Name}}", "--nat-localhostreachable1", "on"]]
  virtualbox_version_file = ".vbox_version"
  vm_name                 = "debian-${var.debian_version}-amd64"
}

build {
  sources = ["source.virtualbox-iso.debian"]

  provisioner "shell" {
    execute_command = "echo 'vagrant' | {{ .Vars }} sudo -E -S bash '{{ .Path }}'"
    scripts         = ["scripts/install.sh", "scripts/cleanup.sh"]
  }

  post-processors {
    post-processor "vagrant" {
      compression_level   = "9"
      output              = "output/debian-${var.debian_release}-${var.debian_version}.box"
    }

    post-processor "artifice" {
      files              = [ "./output/debian-${var.debian_release}-${var.debian_version}.box" ]
    }

    post-processor "vagrant-cloud" {
      access_token    = "${var.cloud_token}"
      box_tag         = "maxhamon/debian${var.debian_major_version}"
      version         = "${var.debian_version}"
    }
  }
}
