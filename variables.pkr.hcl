variable "cloud_token" {
  type        = string
  description = "Vagrant Cloud Token"
  default     = "${env("VAGRANT_CLOUD_TOKEN")}"
  sensitive   = true
}

variable "debian_release" {
  type    = string
  description = "Debian release name"
  default = "bullseye"
}

variable "debian_major_version" {
  type    = number
  description = "Debian release major version"
  default = 11
}

variable "debian_version" {
  type    = string
  description = "Debian release version"
  default = "11.7.0"
}

variable "debian_iso_checksum" {
  type    = string
  description = "Debian release checksum"
  default = "sha256:eb3f96fd607e4b67e80f4fc15670feb7d9db5be50f4ca8d0bf07008cb025766b"
}
